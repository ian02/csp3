import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { Container } from 'react-bootstrap';
import IceCream from '../images/icecream.jpeg';
import Footer from '../components/Footer';
import { FaIceCream } from "react-icons/fa";
 
 
const Home = () => {
   const pageData = {
       title: "Frostbean Cream and Coffee Studio",
       content: "ICE CREAM | COFFEE | TREATS ",
       destination: "/products",
       label: "Browse Menu"
   };
 
   return(
       <React.Fragment>
           <Banner data={pageData}/>
           <Container fluid>
               <br/>
               <img src={IceCream} className='img-fluid'/>
               <br/>
               <br/>
               <h2 className="text-center mb-4"><FaIceCream/>Sweet Treats of the Day<FaIceCream/></h2>
               <Highlights/>
           </Container>
        <Footer />
       </React.Fragment>
   );
}
export default Home;